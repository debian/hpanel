#  This is manual page in Perl POD format. Read more at
#  http://perldoc.perl.org/perlpod.html or run command:
#
#     perldoc perlpod | less
#
#  To check the syntax:
#
#     podchecker *.pod
#
#  Create manual page with command:
#
#    pod2man PAGE.N.pod > PAGE.N

=pod

=head1 NAME

hpanel - a minimalist panel for X

=head1 SYNOPSIS

  hpanel [options]

=head1 DESCRIPTION

hpanel is a minimalist panel for X. Hpanel is version of fspanel
(handles maximized windows better). It contains a small panel that
lists windows and allows switching workspaces for use with a window
manager that supports NETWM specification. Known working window
managers are pekwm and aewm++.

=head1 OPTIONS

=over 4

=item B<-display <display name>>

Specify X display

=item B< -geometry +XPOS+YPOS>

Initial window position.

=item B<-h>

Print available options.

=back

=head1 ENVIRONMENT

None.

=head1 FILES

None.

=head1 SEE ALSO

C<fspanel(1)>

=head1 AUTHORS

This manual page was written by Jari Aalto <jari.aalto@cante.net>, for
the Debian GNU system (but may be used by others).

=cut
